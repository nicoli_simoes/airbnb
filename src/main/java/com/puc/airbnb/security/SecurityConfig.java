package com.puc.airbnb.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableMethodSecurity
public class SecurityConfig {

        @Bean
        public BCryptPasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }


        @Bean
        public AuthenticationManager authManager(HttpSecurity http, BCryptPasswordEncoder bCryptPasswordEncoder, CustomUserDetailsService customUserDetailsService) 
          throws Exception {
            return http.getSharedObject(AuthenticationManagerBuilder.class)
              .userDetailsService(customUserDetailsService)
              .passwordEncoder(bCryptPasswordEncoder)
              .and()
              .build();
        }

        @Bean
        public SecurityFilterChain userFilterChain(HttpSecurity http) throws Exception {
                http
                                .csrf().disable()
                                .antMatcher("/usuarios/cadastro")
                                .authorizeRequests(
                                                authz -> authz
                                                                .anyRequest().permitAll())
                                .httpBasic();
                return http.build();
        }

        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

                http
                                .authorizeHttpRequests(authorize -> authorize
                                                .anyRequest().authenticated())
                                .httpBasic();

                return http.build();
        }

}
