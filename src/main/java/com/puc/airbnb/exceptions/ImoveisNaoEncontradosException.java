package com.puc.airbnb.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ImoveisNaoEncontradosException extends RuntimeException {
    public ImoveisNaoEncontradosException(String msg){
        super(msg);
    }
}
