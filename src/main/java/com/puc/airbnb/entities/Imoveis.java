package com.puc.airbnb.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.puc.airbnb.enums.ImoveisTipoEnum;

import java.util.Objects;

@Document(collection = "imoveis")
public class Imoveis {

    @Id
    private String id;
    private String nomeImovel;
    private ImoveisTipoEnum tipoImovel;

    @DBRef
    private Cidades cidade;
    private Double valorDiaria;

    private Boolean disponivel;

    public Imoveis() {
    }

    public Imoveis(String id, String nomeImovel, ImoveisTipoEnum tipoImovel, Cidades cidade, Double valorDiaria,
            Boolean disponivel) {
        this.id = id;
        this.nomeImovel = nomeImovel;
        this.tipoImovel = tipoImovel;
        this.cidade = cidade;
        this.valorDiaria = valorDiaria;
        this.disponivel = disponivel;
    }

    public String getNomeImovel() {
        return nomeImovel;
    }

    public ImoveisTipoEnum getTipoImovel() {
        return tipoImovel;
    }

    public Cidades getCidade() {
        return cidade;
    }

    public Double getValorDiaria() {
        return valorDiaria;
    }

    public void setNomeImovel(String nomeImovel) {
        this.nomeImovel = nomeImovel;
    }

    public void setTipoImovel(ImoveisTipoEnum tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public void setCidade(Cidades cidade) {
        this.cidade = cidade;
    }

    public void setValorDiaria(Double valorDiaria) {
        this.valorDiaria = valorDiaria;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Imoveis imoveis = (Imoveis) o;
        return Objects.equals(getId(), imoveis.getId()) && Objects.equals(getNomeImovel(), imoveis.getNomeImovel())
                && getTipoImovel() == imoveis.getTipoImovel() && Objects.equals(getCidade(), imoveis.getCidade())
                && Objects.equals(getValorDiaria(), imoveis.getValorDiaria());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNomeImovel(), getTipoImovel(), getCidade(), getValorDiaria());
    }

    public Boolean getDisponivel() {
        return disponivel;
    }

    public void setDisponivel(Boolean disponivel) {
        this.disponivel = disponivel;
    }
}
