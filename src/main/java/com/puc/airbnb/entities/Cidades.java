package com.puc.airbnb.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Document(collection = "cidades")
public class Cidades {

    @Id
    private String id;
    private String nome;
    private String estado;
    private List<Imoveis> listaDeImoveis;


    public Cidades() {
    }

    public Cidades(String id, String nome, String estado) {
        this.id = id;
        this.nome = nome;
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cidades cidades = (Cidades) o;
        return getId().equals(cidades.getId()) && getNome().equals(cidades.getNome()) && getEstado().equals(cidades.getEstado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNome(), getEstado());
    }
}
