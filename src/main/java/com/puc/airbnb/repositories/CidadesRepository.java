package com.puc.airbnb.repositories;

import com.puc.airbnb.entities.Cidades;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CidadesRepository extends MongoRepository<Cidades,String> {
}
