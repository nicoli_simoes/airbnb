package com.puc.airbnb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.puc.airbnb.dto.UsuarioLoginDTO;
import com.puc.airbnb.entities.Usuario;
import com.puc.airbnb.services.UsuarioService;

@Controller
@RequestMapping(value = "/usuarios")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;


    @PostMapping(value = "/cadastro")
    public ResponseEntity<Usuario> cadastrar(@RequestBody Usuario usuario) {
        return ResponseEntity.ok( usuarioService.salvarUsuario(usuario));
    }


    @PostMapping(value = "/login")
    public ResponseEntity<Void> login(@RequestBody UsuarioLoginDTO uDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", usuarioService.login(uDto));

        return ResponseEntity.ok().headers(headers).build();
    }
    
}
