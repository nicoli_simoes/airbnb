package com.puc.airbnb.controllers;

import com.puc.airbnb.entities.Cidades;
import com.puc.airbnb.services.CidadesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/cidades")
public class CidadesController {

    @Autowired
    private CidadesService cidadesService;

    @GetMapping
    public ResponseEntity<List<Cidades>> listarTodas(){
        List<Cidades> lista= cidadesService.listarTodas();
        return ResponseEntity.ok().body(lista);
    }

    @PostMapping("/inserir")
    public ResponseEntity<Cidades> inserir(@RequestBody Cidades cidades) {
        cidades = cidadesService.inserir(cidades);
        URI uri= ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(cidades.getId()).toUri();
        return ResponseEntity.created(uri).body(cidades);

    }
}
