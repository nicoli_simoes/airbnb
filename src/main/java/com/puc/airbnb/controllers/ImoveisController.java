package com.puc.airbnb.controllers;

import com.puc.airbnb.entities.Imoveis;
import com.puc.airbnb.enums.ImoveisTipoEnum;
import com.puc.airbnb.services.ImoveisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/imoveis")
public class ImoveisController {

    @Autowired
    private ImoveisService imoveisService;

    @GetMapping
    public ResponseEntity<List<Imoveis>> listarTodos() {
        List<Imoveis> lista = imoveisService.listarTodos();
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasRole('LOCADOR')")
    @PostMapping("/inserir")
    public ResponseEntity<Imoveis> inserir(@RequestBody Imoveis imoveis) {
        imoveis= imoveisService.inserir(imoveis);
        URI uri= ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(imoveis.getId()).toUri();
        return ResponseEntity.created(uri).body(imoveis);

    }

    @GetMapping("/disponiveis/{tipoImovel}")
    public ResponseEntity<List<Imoveis>> listarImoveisDisponiveisPorTipo(@PathVariable String tipoImovel){
        List<Imoveis> lista = imoveisService.listarDisponiveisPorTipo(tipoImovel);
        return ResponseEntity.ok(lista);
    }
}
