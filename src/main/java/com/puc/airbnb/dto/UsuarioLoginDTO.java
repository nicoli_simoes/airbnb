package com.puc.airbnb.dto;

import com.puc.airbnb.entities.Usuario;

public class UsuarioLoginDTO {
    private String user;
    private String senha;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }


    public Usuario toEntity() {
        Usuario usuario = new Usuario();
        usuario.setUser(this.user);
        usuario.setSenha(this.senha);
        return usuario;
    }

    public UsuarioLoginDTO fromEntity(Usuario usuario) {
        this.user = usuario.getUser();
        this.senha = usuario.getSenha();
        return this;
    }
}
