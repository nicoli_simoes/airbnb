package com.puc.airbnb.services;


import com.puc.airbnb.dto.UsuarioLoginDTO;
import com.puc.airbnb.entities.Imoveis;
import com.puc.airbnb.enums.ImoveisTipoEnum;
import com.puc.airbnb.exceptions.ImoveisNaoEncontradosException;
import com.puc.airbnb.repositories.ImoveisRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ImoveisService {

    @Autowired
    @Lazy
    private UsuarioService usuarioService;

    @Autowired
    private ImoveisRepository imoveisRepository;

    public List<Imoveis> listarTodos() {

        return imoveisRepository.findAll();
    }

    public Imoveis inserir(Imoveis imoveis){
        return imoveisRepository.save(imoveis);
    }

    @Transactional
    public List<Imoveis> listarDisponiveisPorTipo(String tipoImovel) {
        ImoveisTipoEnum imovelEnum = ImoveisTipoEnum.fromString(tipoImovel);
        List<Imoveis> imoveis = imoveisRepository.findImoveisByTipoImovelAndDisponivel(imovelEnum, true);
        return imoveis;
    }


}
