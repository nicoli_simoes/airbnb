package com.puc.airbnb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.puc.airbnb.dto.UsuarioLoginDTO;
import com.puc.airbnb.entities.Usuario;
import com.puc.airbnb.repositories.UsuarioRepository;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    private BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Transactional
    public Usuario salvarUsuario(Usuario usuario) throws RuntimeException {

        if (usuarioRepository.findByUser(usuario.getUser()) != null) {
            throw new RuntimeException("Usuário já cadastrado");
        }

        usuario.setSenha(passwordEncoder().encode(usuario.getSenha()));

        return usuarioRepository.save(usuario);
    }

    @Transactional
    public String login(UsuarioLoginDTO uDto) throws RuntimeException {
        validarUsuario(uDto);
        return "true";
    }

    @Transactional(readOnly = true)
    public void validarUsuario(UsuarioLoginDTO uDto) throws RuntimeException {
        Usuario usuario = usuarioRepository.findByUser(uDto.getUser());
        if (usuario == null) {
            throw new RuntimeException("Usuário não cadastrado");
        }
        if (!usuario.getSenha().equals(uDto.getSenha())) {
            throw new RuntimeException("Senha inválida");
        }
    }
}
