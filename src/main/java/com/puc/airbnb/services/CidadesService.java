package com.puc.airbnb.services;


import com.puc.airbnb.entities.Cidades;
import com.puc.airbnb.entities.Imoveis;
import com.puc.airbnb.repositories.CidadesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadesService {

    @Autowired
    private CidadesRepository cidadesRepository;

    public List<Cidades> listarTodas(){

        return cidadesRepository.findAll();
    }

    public Cidades inserir(Cidades cidade) {

        return cidadesRepository.save(cidade);
    }
}
