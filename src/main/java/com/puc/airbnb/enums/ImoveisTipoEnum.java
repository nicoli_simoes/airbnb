package com.puc.airbnb.enums;

public enum ImoveisTipoEnum {
    APARTAMENTO("Apartamento"),
    CASA("Casa"),
    QUARTO("Quarto");

    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    private ImoveisTipoEnum(String tipo) {
        this.tipo = tipo;
    }


    // return ennum from string
    public static ImoveisTipoEnum fromString(String text) {
        for (ImoveisTipoEnum b : ImoveisTipoEnum.values()) {
            if (b.tipo.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
