package com.puc.airbnb.enums;

public enum UsuarioTipoEnum {
    LOCADOR("LOCADOR"),
    LOCATARIO("LOCATARIO");

    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    private UsuarioTipoEnum(String tipo) {
        this.tipo = tipo;
    }

    // return ennum from string
    public static UsuarioTipoEnum fromString(String text) {
        for (UsuarioTipoEnum b : UsuarioTipoEnum.values()) {
            if (b.tipo.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String s = "ROLE_" + this.tipo;
        return s;
    }

}
